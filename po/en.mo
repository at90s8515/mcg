��    D      <  a   \      �     �     �     �                    #     *     3     B     U     ]     s  
   �     �     �  _   �     �          :     A     G     O     U     Z     m     u     �     �     �  	   �     �     �     �     �     �     �     �                    .     >     U  !   j     �     �     �  
   �     �     �     �     	     	     8	     U	     [	     m	     t	     y	     	     �	     �	     �	     �	     �	     �	  �  �	     g     s     �     �     �     �     �     �     �     �     �     �     �  
               _   %     �     �     �     �     �     �     �     �     �     �     	          '  	   <     F     K     R     [     a     f     {     �     �     �     �     �     �  !   �  )        5  
   ;  
   F     Q     X     v     �     �     �     �     �     �          
            	   &     0     9     A     M               "   (      &      ,            )       4      .                   /       @   9   A                 =         5                          !          0   D           $   6             B                #   ?         
       >   %   +         3   :       	   *      1      7                             2   8   <   ;   '   C               -           <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter hostname or IP address Enter password or leave blank Error: File: General Host: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search Library Search the library Seconds Seconds played Seconds running Select multiple albums Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts Songs Sort Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Library panel Switch to the Playlist panel Title Toggle Fullscreen cancel play queue remove search library sort by artist sort by title sort by year {} feat. {} {}:{} minutes Project-Id-Version: CoverGrid (mcg)
PO-Revision-Date: 2020-10-24 14:40+0200
Last-Translator: coderkun <olli@suruatoel.xyz>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: mcg
X-Poedit-SearchPath-1: data/ui
 <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter hostname or IP address Enter password or leave blank Error: File: General Host: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search Library Search the library Seconds Seconds Seconds running Select multiple albums Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts (this dialog) Songs Sort order Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Cover panel Switch to the Playlist panel Title Toggle fullscreen cancel play queue remove search library by Artist by Title by Year {} feat. {} {}:{} minutes 